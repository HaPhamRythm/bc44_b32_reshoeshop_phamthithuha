import React, { Component } from "react";
import Item from "./Item";

export default class List extends Component {
  render() {
    return (
      <div className="col-8 row">
        {this.props.movies.map((item, index) => {
          return (
            <Item
              key={index}
              movie={item}
              handleDetailItem={this.props.handleDetailList}
            />
          );
        })}
      </div>
    );
  }
}

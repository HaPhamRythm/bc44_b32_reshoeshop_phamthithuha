import React, { Component } from "react";

export default class EventHandling extends Component {
  handleSayHello = () => {
    console.log("he leu");
  };
  handleGoodbye = (user) => {
    console.log(`Goodbye ${user}`);
  };
  render() {
    return (
      <div>
        <h2>Event Handling</h2>
        <button className="btn btn-success" onClick={this.handleSayHello}>
          Say Hello
        </button>
        <br />
        <button
          className="btn btn-primary"
          onClick={() => {
            this.handleGoodbye("baby");
          }}
        >
          Goodbye someone
        </button>
      </div>
    );
  }
}

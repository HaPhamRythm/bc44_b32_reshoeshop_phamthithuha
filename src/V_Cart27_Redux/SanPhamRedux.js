import React, { Component } from "react";
import { ADD_TO_CART } from "./redux/constant/cartConstant";
import { connect } from "react-redux";

class SanPhamRedux extends Component {
  render() {
    let { image, name, price } = this.props.shoe;
    return (
      <div className="col-4 mb-3">
        <div className="card text-left h-100">
          <img
            className="card-img-top"
            src={image}
            width={150}
            height={200}
            alt=""
          />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{price}</p>
          </div>
          <button
            onClick={() => {
              this.props.handleBuy(this.props.shoe);
            }}
            className="btn btn-success"
          >
            Buy
          </button>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleBuy: (shoe) => {
      let newShoe = { ...shoe, number: 1 };
      dispatch({
        type: ADD_TO_CART,
        payload: newShoe,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(SanPhamRedux);

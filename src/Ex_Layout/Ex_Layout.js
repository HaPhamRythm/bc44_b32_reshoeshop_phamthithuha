import React, { Component } from "react";
import Content from "./Content";
import Navigation from "./Navigation";
import Header from "./Header";
import Footer from "./Footer";

export default class Ex_Layout extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="row">
          <div className="col-4 p-0">
            <Navigation />
          </div>
          <div className="col-8 p-0">
            <Content />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { data, handleCLickToItemShoe, handleBuyToItemShoe } = this.props;
    let { image, name } = data;
    return (
      <div className="col-6 mb-3">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
          </div>
          <div>
            <button
              className="btn btn-primary mx-5"
              onClick={() => {
                handleCLickToItemShoe(data);
              }}
            >
              See More ...
            </button>
            <button
              onClick={() => {
                handleBuyToItemShoe(data);
              }}
              className="btn btn-success"
            >
              Buy
            </button>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import ItemGlass from "./ItemGlass";

export default class List extends Component {
  renderList = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemGlass
          data={item}
          key={index}
          handleChangeToItem={this.props.handleChangeToList}
        />
      );
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row mt-5 ">{this.renderList()}</div>;
      </div>
    );
  }
}

import { shoeArr } from "../../../Re_ShoeShop_Redux/data";
import {
  BUY,
  CHANGE_QUANTITY,
  DELETE,
  VIEW_DETAIL,
} from "../constant/shoeConstant";

let initialState = {
  shoeArr: shoeArr,
  detail: shoeArr[0],
  cart: [],
};

export const shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case VIEW_DETAIL: {
      state.detail = payload;
      return { ...state };
    }
    case DELETE: {
      let cloneCart = state.cart.filter((item) => item.id !== payload);
      return { ...state, cart: cloneCart };
    }

    case BUY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id == payload.id);
      if (index == -1) {
        let newShoe = { ...payload, number: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id == payload.id);
      cloneCart[index].number = cloneCart[index].number + payload.option;
      if (cloneCart[index].number == 0) {
        cloneCart.splice(index, 1);
      }
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};

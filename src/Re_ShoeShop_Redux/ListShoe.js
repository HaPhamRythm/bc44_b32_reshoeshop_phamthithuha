import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
import { connect } from "react-redux";
class ListShoe extends Component {
  renderList = () => {
    return this.props.shoeList.map((item, index) => {
      return (
        <ItemShoe
          key={index}
          data={item}
          // handleCLickToItemShoe={this.props.handleClickToListShoe}
          // handleBuyToItemShoe={this.props.handleBuyToListShoe}
        />
      );
    });
  };
  render() {
    return <div className="row col-6">{this.renderList()}</div>;
  }
}
let mapStateToProps = (state) => {
  return { shoeList: state.shoeReducer.shoeArr };
};
export default connect(mapStateToProps)(ListShoe);

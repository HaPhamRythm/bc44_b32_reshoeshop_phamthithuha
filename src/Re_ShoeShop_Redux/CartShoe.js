import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY, DELETE } from "./redux/constant/shoeConstant";

class CartShoe extends Component {
  render() {
    let { handleDeleteToCart, cart, handleNumberToCart } = this.props;
    return (
      <div className="col-6">
        <h3>Cart</h3>
        <table class="table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Number</th>
              <th>Price</th>
              <th>Amount</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((item, index) => {
              return (
                <tr key={index}>
                  <td>
                    <img style={{ width: 50 }} src={item.image} alt="" />
                  </td>
                  <td>{item.name}</td>
                  <td>
                    <button
                      onClick={() => {
                        handleNumberToCart(item.id, -1);
                      }}
                      className="btn btn-info"
                    >
                      -
                    </button>
                    <strong className="mx-1">{item.number}</strong>
                    <button
                      onClick={() => {
                        handleNumberToCart(item.id, 1);
                      }}
                      className="btn btn-success"
                    >
                      +
                    </button>
                  </td>
                  <td>{item.price}</td>
                  <td>{item.price * item.number}</td>

                  <td>
                    <button
                      onClick={() => {
                        handleDeleteToCart(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      x
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return { cart: state.shoeReducer.cart };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDeleteToCart: (id) => {
      dispatch({
        type: DELETE,
        payload: id,
      });
    },
    handleNumberToCart: (id, option) => {
      dispatch({
        type: CHANGE_QUANTITY,
        payload: { id, option },
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);

import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderArr = () => {
    return this.props.arrList.map((item, index) => {
      return (
        <ItemShoe
          handleDetailItem={this.props.handleDetailList}
          key={index}
          arrItem={item}
          handleBuyItem={this.props.handleBuyList}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderArr()}</div>;
  }
}

import logo from "./logo.svg";
import "./App.css";
import DemoHeader from "./DemoComponent/DemoHeader";
import Ex_Layout from "./Ex_Layout/Ex_Layout";
import DataBinding from "./DataBinding/DataBinding";
import EventHandling from "./EventHandling/EventHandling";
import DemoState from "./DemoState/DemoState";
import Ex_State_Car from "./Ex_State_Car/Ex_State_Car";
import BaiTapLayoutComponent from "./BaiTapLayoutComponent/BaiTapLayoutComponent";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import DemoProps from "./DemoProps/DemoProps";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
import Re_ShoeShop from "./Re_ShoeShop/Re_ShoeShop";
import HW_Glasses from "./HW_Glasses/HW_Glasses";
import Ex_Movie from "./Ex_Movie/Ex_Movie";
import Demo_MiniRedux from "./Demo_MiniRedux/Demo_MiniRedux";
import Re_MiniRedux from "./Re_MiniRedux/Re_MiniRedux";
import Ex_ShoeShop_Redux from "./Ex_ShoeShop_Redux/Ex_ShoeShop_Redux";
import Re_ShoeShop_Redux from "./Re_ShoeShop_Redux/Re_ShoeShop_Redux";
import V_Cart27_Redux from "./V_Cart27_Redux/V_Cart27_Redux";
import Re_V_Cart27_Redux from "./Re_V_Cart27_Redux/Re_V_Cart27_Redux";
import DemoLifeCycle from "./DemoLifeCycle/DemoLifeCycle";
import Re_DemoLifeCycle from "./Re_DemoLifeCycle/Re_DemoLifeCycle";
import ManagementProduct from "./ReactForm/ManagementProduct";
import FormQuanLySV from "./QuanLySV/FormQuanLySV";
import Re_ShoeShopLifeCycleAxios from "./Re_ShoeShopLifeCycleAxios/Re_ShoeShopLifeCycleAxios";
import Re_ReactForm from "./Re_ReactForm/Re_ReactForm";
import ReactForm2 from "./ReactForm2/ReactForm2";

function App() {
  return (
    <div className="App">
      {/* <DemoClass />
      <DemoFunction></DemoFunction> */}
      {/* <DemoHeader /> */}
      {/* <Ex_Layout /> */}
      {/* <DataBinding /> */}
      {/* <EventHandling /> */}
      {/* <DemoState /> */}
      {/* <Ex_State_Car /> */}
      {/* <BaiTapLayoutComponent /> */}
      {/* <RenderWithMap /> */}
      {/* <DemoProps /> */}
      {/* <Ex_ShoeShop /> */}
      {/* <Re_ShoeShop /> */}
      {/* <HW_Glasses /> */}
      {/* <Ex_Movie /> */}
      {/* <Demo_MiniRedux /> */}
      {/* <Re_MiniRedux /> */}
      {/* <Ex_ShoeShop_Redux /> */}
      {/* <Re_ShoeShop_Redux /> */}
      {/* <V_Cart27_Redux /> */}
      {/* <Re_V_Cart27_Redux /> */}
      {/* <DemoLifeCycle /> */}
      {/* <Re_DemoLifeCycle /> */}
      {/* <ManagementProduct /> */}
      {/* <FormQuanLySV /> */}
      {/* <Re_ShoeShopLifeCycleAxios /> */}
      <Re_ReactForm />
      {/* <ReactForm2 /> */}
    </div>
  );
}

export default App;

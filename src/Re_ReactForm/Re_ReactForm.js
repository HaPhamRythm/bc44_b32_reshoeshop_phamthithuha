import React, { Component } from "react";
import ProductList from "./ProductList";
import { ADD_NEW_STUDENT, ONCHANGE } from "./redux/constant/constantStudents";
import { connect } from "react-redux";

class Re_ReactForm extends Component {
  state = {
    student: {
      id: "",
      name: "",
      phone: "",
      email: "",
    },
    errors: {
      id: "",
      name: "",
      phone: "",
      email: "",
    },
    editedStudent: {
      id: "",
      name: "",
      phone: "",
      email: "",
    },
  };

  handleOnchange = (event) => {
    const { id, value, pattern, type } = event.target;
    let { student, errors, editedStudent } = this.state;
    let newStudent = {};
    let newEditStudent = {};

    if (editedStudent.id || type === "id") {
      newStudent = { ...student, ["id"]: editedStudent.id };
      newEditStudent = { ...editedStudent, ["id"]: "" };
    } else {
      newStudent = { ...student, [id]: value };
      newEditStudent = { ...editedStudent, [id]: "" };
    }

    const newErrors = { ...errors };
    if (!value.trim()) {
      newErrors[id] = "Please fill the blank";
    } else {
      if (pattern == "^[0-9]{4,6}$") {
        let regex = /^[0-9]{4,6}$/;
        const valid = regex.test(value);
        if (!valid) {
          newErrors.id = "Please enter 4-6 integers";
        } else {
          newErrors.id = "";
        }
      }

      if (pattern == "^[a-zA-Z ]+$") {
        let alphaExp = /^[a-zA-Z ]+$/;
        if (!alphaExp.test(value)) {
          newErrors.name = "Please enter only alphabets";
        } else {
          newErrors.name = "";
        }
      }
      if (pattern == "^[0-9]{10}$") {
        let phoneNum = /^[0-9]{10}$/;
        const valid = phoneNum.test(value);
        if (!valid) {
          newErrors.phone = "Please enter 10 integers";
        } else {
          newErrors.phone = "";
        }
      }
      if (type === "email") {
        let email =
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!email.test(value)) {
          newErrors.email = "Email is in wrong format";
        } else {
          newErrors.email = "";
        }
      }
    }

    // let hasSelectStudent = true;
    // Object.values(this.state.editedStudent).forEach((value) => {
    //   if (value) {
    //   }
    // });
    // if (!hasSelectStudent) {
    // }

    console.log(newEditStudent);
    this.setState({
      student: newStudent,
      errors: newErrors,
      editedStudent: newEditStudent,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    let valid = true;
    let newErrors = { ...this.state.errors };

    valid = Object.values(newErrors)
      .map((value) => {
        return Boolean(value.length);
      })
      .every((every) => every === false);

    valid = Object.values(this.state.student)
      .map((value) => {
        return Boolean(value.length);
      })
      .every((every) => every === true);

    if (valid) {
      // const { id, value } = event.target;
      // const newStudent = { ...this.state.student, [id]: value };
      // this.setState({ student: newStudent });
      this.props.handleAdd(this.state.student);

      this.setState({
        student: {
          id: "",
          name: "",
          phone: "",
          email: "",
        },
      });
    } else {
      Object.keys(this.state.student).forEach((key) => {
        if (this.state.student[key].length === 0) {
          newErrors[key] = "Please fill the blank";
        }
      });
      // if (hasEdit) {
      // } else {
      //   Object.keys(this.state.student).forEach((key) => {
      //     if (this.state.student[key].length === 0) {
      //       newErrors[key] = "Please fill the blank";
      //     }
      //   });
      // }

      let newsStudent = { ...this.state, errors: newErrors };
      this.setState(newsStudent);
    }
  };

  static getDerivedStateFromProps(props, state) {
    console.log("editedStudent props", props);
    console.log("student state", state);
    // Neu ton tai editedStudent
    if (props.editedStudent) {
      // let hasStudent = false;

      // Khi nguoi dung type thi set lai state, neu state student
      // ton tai thi khong can gan lai gia tri cho editStudent
      let checkValues = Object.values(state.student)
        .map((value) => {
          return Boolean(value);
        })
        .every((every) => every === false);

      let checkErrors = Object.values(state.errors)
        .map((value) => {
          return Boolean(value);
        })
        .every((every) => every === false);

      if (checkValues && checkErrors) {
        state.editedStudent = { ...props.editedStudent };
      }
    }
  }

  render() {
    let { handleAdd } = this.props;
    let { editedStudent, student, errors } = this.state;
    let { id, name, phone, email } = student;
    // let { id, name, phone, email } = student;
    console.log("editedStudent", editedStudent);
    console.log("student", student);
    return (
      <div className="container">
        <div className=" text-left">
          <h3>Students Information</h3>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-6 mb-3">
                <label html For="">
                  ID
                </label>
                <input
                  value={id ? id : editedStudent.id}
                  type="text"
                  id="id"
                  name="id"
                  className="form-control"
                  onChange={this.handleOnchange}
                  pattern="^[0-9]{4,6}$"
                />

                <span className="text text-danger">{errors.id}</span>
              </div>

              <div className="col-6 mb-3">
                <label htmlFor="">Name</label>
                <input
                  value={name ? name : editedStudent.name}
                  type="text"
                  id="name"
                  name="name"
                  className="form-control"
                  onChange={this.handleOnchange}
                  pattern="^[a-zA-Z ]+$"
                />
                <span className="text text-danger">{errors.name}</span>
              </div>

              <div className="col-6 mb-3">
                <label htmlFor="">Phone Number</label>
                <input
                  value={phone ? phone : editedStudent.phone}
                  type="text"
                  id="phone"
                  name="phone"
                  className="form-control"
                  onChange={this.handleOnchange}
                  pattern="^[0-9]{10}$"
                />
                <span className="text text-danger">{errors.phone}</span>
              </div>

              <div className="col-6 mb-3">
                <label htmlFor="">Email</label>
                <input
                  value={email ? email : editedStudent.email}
                  type="email"
                  id="email"
                  name="email"
                  className="form-control"
                  onChange={this.handleOnchange}
                  pattern="^(([^<>()[\]\\.,;:\s@']+(\.[^<>()[\]\\.,;:\s@']+)*)|.('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$"
                />
                <span className="text text-danger">{errors.email}</span>
              </div>

              <button
                // onClick={() => {
                //   handleAdd(this.state.student);
                // }}
                className="btn btn-success"
              >
                Add New Student
              </button>
            </div>
          </form>
        </div>

        <h3>List of Students</h3>
        <ProductList />
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return { editedStudent: state.studentsReducer.editedStudent };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAdd: (student) => {
      dispatch({
        type: ADD_NEW_STUDENT,
        payload: student,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Re_ReactForm);

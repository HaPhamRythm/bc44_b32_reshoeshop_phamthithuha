import React, { Component } from "react";
import { connect } from "react-redux";
import {
  DELETE_FROM_CART,
  INCREASE_DECREASE,
} from "./redux/constant/constantCart";

class CartModel extends Component {
  renderCart = () => {
    return this.props.cartList.map((item, index) => {
      return (
        <tr key={index}>
          <td>
            <img src={item.image} width={100} height={100} alt="" />
          </td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleInDe(index, 1);
              }}
              className="btn btn-success"
            >
              +
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handleInDe(index, -1);
              }}
              className="btn btn-warning"
            >
              -
            </button>
          </td>
          <td>{item.price * item.number}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(index);
              }}
              className="btn-danger"
            >
              X
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="col-6">
        <h2>CartModel</h2>
        <table class="table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Number</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
          <tfoot>
            <td colSpan="3"></td>
            <td>Total bill</td>
            <td>
              {this.props.cartList.reduce((totalBill, item, index) => {
                return (totalBill += item.number * item.price);
              }, 0)}
            </td>
          </tfoot>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return { cartList: state.cartReducer.cart };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (index) => {
      dispatch({
        type: DELETE_FROM_CART,
        payload: index,
      });
    },
    handleInDe: (index, inDe) => {
      dispatch({
        type: INCREASE_DECREASE,
        inDe,
        index,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CartModel);

import React, { Component } from "react";
import { shoeArr } from "../V_Cart27_Redux/data";
import Item from "./Item";

export default class ListShoes extends Component {
  renderList = () => {
    return shoeArr.map((item, index) => {
      return <Item key={index} shoe={item} />;
    });
  };
  render() {
    return (
      <div className="col-6">
        <h2>List of Shoes</h2>
        <div className="row">{this.renderList()}</div>
      </div>
    );
  }
}

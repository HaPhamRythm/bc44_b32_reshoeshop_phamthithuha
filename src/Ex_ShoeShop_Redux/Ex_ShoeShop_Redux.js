import React, { Component } from "react";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import { shoeArr } from "./data";
import CartShoe from "./CartShoe";

export default class Ex_ShoeShop_Redux extends Component {
  render() {
    return (
      <div>
        <h2>ShoeShop</h2>
        <div className="row">
          <CartShoe />
          <ListShoe />
        </div>

        <DetailShoe />
      </div>
    );
  }
}

import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY, DELETE_SHOE } from "./redux/constant/shoeConstant";

class CartShoe extends Component {
  renderCart = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.name}</td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item, -1);
              }}
              className="btn btn-warning"
            >
              -
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item, 1);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>{item.price * item.number}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
              className="btn btn-danger"
            >
              x
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <h3>Shopping Cart</h3>
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Image</th>
              <th>Price</th>
              <th>Number</th>
              <th>Amount</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return { cart: state.shoeReducer.cart };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (idShoe) => {
      dispatch({
        type: DELETE_SHOE,
        payload: idShoe,
      });
    },
    handleChangeQuantity: (shoe, option) => {
      dispatch({
        type: CHANGE_QUANTITY,
        payload: { shoe, option },
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);

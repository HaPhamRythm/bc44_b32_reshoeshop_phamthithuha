import React, { Component } from "react";
import TableSV from "./TableSV";
import FormSV from "./FormSV";

export default class FormQuanLySV extends Component {
  render() {
    return (
      <div className="container">
        <h3>FormQuanLySV</h3>
        <FormSV />
        <TableSV />
      </div>
    );
  }
}

import React, { Component } from "react";
import { connect } from "react-redux";
import { TANG, GIAM } from "./Redux/constant/numberConstant";

class Re_MiniRedux extends Component {
  render() {
    console.log(this.props);
    return (
      <div>
        <h2>Re_MiniRedux</h2>
        <button
          onClick={() => {
            this.props.handleGiam(5);
          }}
          className="btn btn-warning"
        >
          -
        </button>
        <strong className="mx-3">{this.props.amount}</strong>
        <button onClick={this.props.handleTang} className="btn btn-danger">
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return { amount: state.numberReducer.soLuong };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleTang: () => {
      dispatch({
        type: TANG,
        payload: 1,
      });
    },
    handleGiam: (number) => {
      dispatch({
        type: GIAM,
        payload: number,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Re_MiniRedux);

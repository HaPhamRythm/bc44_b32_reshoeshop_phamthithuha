import React, { Component } from "react";
import ProductList from "./ProductList";
export default class ManagementProduct extends Component {
  state = {
    values: {
      id: "",
      image: "",
      name: "",
      type: "Apple",
      price: 0,
      description: "",
    },
    errors: {
      id: "",
      image: "",
      name: "",

      price: "",
      description: "",
    },
    listProducts: [
      {
        id: "123",
      },
    ],
    edit: null,
  };

  handleOnchange = (event) => {
    const { id, value, pattern } = event.target;
    // console.log("id", id, "value", value);
    const newValue = { ...this.state.values, [id]: value };
    const newError = { ...this.state.errors };
    if (!value.trim()) {
      newError[id] = "Please fill the blank";
    } else {
      if (pattern) {
        const regex = new RegExp(pattern);
        const valid = regex.test(value);
        if (!valid) {
          newError[id] = id + ": invalid format";
        } else {
          newError[id] = "";
        }
      }
    }
    this.setState({ values: newValue, errors: newError });
  };

  handleOnBlur = (event) => {
    const { id, value, pattern } = event.target;
    // console.log("id", id, "value", value);
    const newValue = { ...this.state.values, [id]: value };
    const newError = { ...this.state.errors };
    if (!value.trim()) {
      newError[id] = "Please fill the blank";
    } else {
      if (pattern) {
        const regex = new RegExp(pattern);
        const valid = regex.test(value);
        if (!valid) {
          newError[id] = id + ": invalid format";
        } else {
          newError[id] = "";
        }
      }
    }
    this.setState({ values: newValue, errors: newError });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    let valid = true;
    Object.values(this.state.errors).forEach((item) => {
      if (item.length > 0) {
        valid = false;
      }
      if (valid) {
        const newListProducts = [...this.state.listProducts, this.state.values];
        this.setState({ listProducts: newListProducts });
      }
    });
    const newProducts = [...this.state.listProducts, this.state.values];
    this.setState({ listProducts: newProducts });
    console.log(this.state.values);
  };

  handleDelete = (idProduct) => {
    const index = this.state.listProducts.findIndex((item) => {
      return item.id === idProduct;
    });
    if (index !== -1) {
      const newListProducts = [...this.state.listProducts];
      newListProducts.splice(index, 1);
      this.setState({ listProducts: newListProducts });
    }
  };

  handleStartEdit = (idProduct) => {
    const findProduct = this.state.listProducts.find((item) => {
      return item.id === idProduct;
    });
    this.setState({ edit: findProduct, values: findProduct });
  };

  handleEdit = () => {
    const index = this.state.listProducts.findIndex((item) => {
      return item.id === this.state.edit.id;
    });
    const newProducts = [...this.state.listProducts];
    newProducts[index] = this.state.values;
    this.setState({
      listProducts: newProducts,
      values: {
        id: "",
        image: "",
        name: "",
        type: "Apple",
        price: 0,
        description: "",
      },
      edit: null,
    });
  };
  render() {
    let { values, errors, listProducts, edit } = this.state;
    let { id, image, name, type, price, description } = values;
    return (
      <div className="container text-left">
        <h3 className="text-center">Management Products</h3>
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-6 mt-3">
              <label>ID</label>
              <input
                value={id}
                type="text"
                id="id"
                name="id"
                className="form-control"
                pattern="^[0-9a-zA-Z]{1,5}$"
                onChange={this.handleOnchange}
                onBlur={this.handleOnBlur}
                disabled={edit}
              />
              {errors.id && (
                <span className="text text-danger">{errors.id}</span>
              )}
            </div>
            <div className="col-6 mt-3">
              <label>Image</label>
              <input
                value={image}
                type="text"
                id="image"
                name="image"
                className="form-control"
                onChange={this.handleOnchange}
                onBlur={this.handleOnBlur}
              />
              {errors.image && (
                <span className="text text-danger">{errors.image}</span>
              )}
            </div>
            <div className="col-6 mt-3">
              <label>Name</label>
              <input
                value={name}
                type="text"
                id="name"
                name="name"
                className="form-control"
                onChange={this.handleOnchange}
                onBlur={this.handleOnBlur}
              />
              {errors.name && (
                <span className="text text-danger">{errors.name}</span>
              )}
            </div>
            <div className="col-6 mt-3">
              <label>Product Type</label>
              <select
                value={type}
                id="type"
                name="type"
                className="form-control"
                onChange={this.handleOnchange}
              >
                <option value="Apple">Apple</option>
                <option value="Samsung">Samsung</option>
              </select>
            </div>
            <div className="col-6 mt-3">
              <label>Price</label>
              <input
                value={price}
                type="text"
                id="price"
                name="price"
                className="form-control"
                pattern="^[0-9]*$"
                onChange={this.handleOnchange}
                onBlur={this.handleOnBlur}
              />
              {errors.price && (
                <span className="text text-danger">{errors.price}</span>
              )}
            </div>
            <div className="col-6 mt-3">
              <label>Description</label>
              <input
                value={description}
                type="text"
                id="description"
                name="description"
                className="form-control"
                onChange={this.handleOnchange}
                onBlur={this.handleOnBlur}
              />
              {errors.description && (
                <span className="text text-danger">{errors.description}</span>
              )}
            </div>
            <div className="col-12 mt-3">
              {edit ? (
                <button
                  type="button"
                  onClick={this.handleEdit}
                  className="btn btn-warning mr-3"
                >
                  Edit
                </button>
              ) : (
                <button className="btn btn-danger ">Add</button>
              )}
            </div>
          </div>
        </form>
        <ProductList
          data={listProducts}
          handleDelete={this.handleDelete}
          handleStartEdit={this.handleStartEdit}
        />
      </div>
    );
  }
}

import React, { Component, PureComponent } from "react";

export default class DemoLC extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      count: 1,
    };
    console.log("constructor Child");
  }

  static getDerivedStateFromProps(props, state) {
    console.log("getDerivedStateFromProps Child", props, state);
    return { ...state, numberChild: props.number * 2 };
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log(
  //     "shouldComponentUpdate Child",
  //     nextProps,
  //     nextState,
  //     this.props
  //   );
  //   if (this.props.number === nextProps.number) {
  //     return false;
  //   }
  //   return true;
  // }
  render() {
    console.log("render");
    return (
      <div>
        DemoLC
        <div>{this.state.numberChild}</div>
      </div>
    );
  }
  componentDidMount() {
    console.log("componentDidMount Child");
  }
}

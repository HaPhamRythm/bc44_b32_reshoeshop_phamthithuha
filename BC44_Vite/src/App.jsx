import { useState } from "react";
import "./App.css";
import Home from "./pages/Home";
import DemoUseEffect from "./pages/DemoUseEffect/DemoUseEffect";
import DemoUseCallBack from "./pages/DemoUseCallBack/DemoUseCallBack";

function App() {
  // useState dung de khoi tao state component
  //setState dung co che replace, not merge nhu this.state
  const initialCount = 0;
  const [count, setCount] = useState(initialCount);
  const [like, setLike] = useState(1);
  const [person, setPerson] = useState({ name: "Hau", age: "26" });
  console.log("count", count);

  const handleClickCount = () => {
    setCount(count + 1);
    setCount((previousState) => previousState + 1);
  };
  return (
    <>
      <div className="container">
        {/* <Home />
        <p>Count: {count}</p>
        <button onClick={handleClickCount}>Increase</button> */}
        {/* <DemoUseEffect /> */}
        <DemoUseCallBack />
      </div>
    </>
  );
}

export default App;

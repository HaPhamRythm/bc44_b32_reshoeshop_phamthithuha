import React, { useEffect } from "react";

export default function DemoUseEffect() {
  const [count, setCount] = useState(0);
  // useEffect nhan vao 2 tham so: 1 callback function, 2 dependencies
  // useEffect(()=> {}, [dependencies])
  // componentDidMount,
  // TH1: su dung 1 tham so callback, dai dien cho componentDidUpdate
  useEffect(() => {
    if (count < 10) {
      console.log("useEffect");
      setCount(count + 1);
    }
  });
  // TH2: su dung 2 tham so callback va dependencies, chay 1 lan duy nhat, dai dien cho componentDidMount
  useEffect(() => {
    //call api
    console.log("useEffect rong");
  }, []);

  // TH3: su dung 2 tham so callback va dependencies co nhieu value trong do, chay 1 lan duy nhat, dai dien cho componentDidMount va phu thuoc value cua dependencies
  useEffect(() => {
    //call api
    console.log("useEffect rong");
  }, [count]);

  // TH4: su dung 2 tham so callback (co return) va dependencies [], dai dien cho ComponentWillUnmount
  useEffect(() => {
    document.addEventListener("scroll", handleScroll);
    return () => {
      console.log("useEffect unmount");
    };
  }, []);

  return (
    <div className="container">
      DemoUseEffect
      <p>Count: {count}</p>
      <button onClick={() => setCount(count + 1)} className="btn btn-primary">
        Increase
      </button>
    </div>
  );
}

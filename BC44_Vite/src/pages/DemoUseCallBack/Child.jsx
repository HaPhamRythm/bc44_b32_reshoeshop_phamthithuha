import React, { memo } from "react";
// memo se kiem tra props cu va moi co thay doi ko, neu co render lai Component
// memo nhan 2 tham so 1: component; 2: 1 function co 2 tham so (previousProps, newProps)
// memo su dung shallow ===
const Child = (props) => {
  console.log("Child Render", props);
  return <div>Child</div>;
};

export default memo(Child, (preProps,newProps));
